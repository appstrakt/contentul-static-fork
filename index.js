var
    contentful = require('contentful');
    contentfulMan = require('contentful-management');
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    q = require('q'),
    chalk = require('chalk'),
    rimraf = require('rimraf'),
    consolidate = require('consolidate'),
    path = require('path'),
    marked = require('marked'),
    nunjucks = require('nunjucks'),
    nunjucksDate = require('nunjucks-date'),
    markdown = require('nunjucks-markdown');

module.exports = (function() {
    var contentTypes = {
        array: [],
        byId: {}
    };

    var options = {
        engine: 'nunjucks',
        templates: 'templates',
        apiconfig: {
            space: null,
            accessToken: null,
            secure: true,
            host: 'cdn.contentful.com'
        },
        apiconfigManagement: {
            space: null,
            accessToken: null
        }
    };

    var writeToFile = function() {
        var deferred = q.defer();
        var filename = options.dest;
        var filepath = process.cwd() + '/' + filename;
        var directory = path.dirname(filepath);
        var contents = JSON.stringify(db, null, 2);

        mkdirp(directory, function(err) {
            if(err) {
                deferred.reject(err);
                return;
            }

            try {
                fs.writeFileSync(filepath, contents, 'utf8');
            } catch (err) {
                deferred.reject(err);
                return;
            }
            deferred.resolve();
        });
        return deferred.promise;
    };


    var contentfulStatic = {

        config: function( optionsObject ) {
            options.templates = optionsObject.templates || options.templates;
            options.engine = optionsObject.engine || options.engine;
            options.apiconfig.space = optionsObject.space || options.apiconfig.space;
            options.apiconfig.accessToken = optionsObject.accessToken || options.apiconfig.accessToken;
            options.apiconfig.secure = optionsObject.secure || options.apiconfig.secure;
            options.apiconfig.host = optionsObject.host || options.apiconfig.host;

            options.apiconfigManagement.space = optionsObject.space || options.apiconfigManagement.space;
            options.apiconfigManagement.accessToken = optionsObject.accessTokenManagement || options.apiconfigManagement.accessToken;

        },

        /**
         * Transform markdown fields to html. You can choose to supply a callback or just use the promise
         * that is returned.
         *
         * @param {Object} data The content.
         * @param {Function} callback (optional) a callback function(err, content)
         * @return {Promise} a promise that resolves to the content.
         */
        transformMarkdownToHtml: function(data, callback) {
            // Get markdown fields
            var markdownFields = {};
            data.contentTypes.forEach(function(contentType, index) {
                markdownFields[contentType.sys.id] = [];
                contentType.fields.forEach(function(field,index){
                    if(field.type === "Text") {
                        markdownFields[contentType.sys.id].push(field.id);
                    }
                })
            });

            // for all entries,
            var queries = [];
            data.entries['en-US'].forEach(function(entry, index) {
                var entryContentType = entry.sys.contentType.sys.id;
                var entryMarkdownFields = markdownFields[entryContentType];
                if (entryMarkdownFields.length > 0) {
                    for (var i = 0; i < entryMarkdownFields.length; i++) {
                        var field = entryMarkdownFields[i];
                        if (entry.fields[field] !== undefined) {
                            queries.push(marked(entry.fields[field], function(err, html) {
                                entry.fields[field] = html;
                            }));
                        }
                    };
                };
            });

            var promise = q.all(queries).then( function(response) {
                return data;
            });

            if (callback) {
                promise.then(function(db) {
                    process.nextTick(function() {
                        callback(undefined, db);
                    });
                }, callback);
            }
            return promise;
        },

        /**
         * Sync content from contentful. You can choose to supply a callback or just use the promise
         * that is returned.
         *
         * @param {Function} callback (optional) a callback function(err, content)
         * @return {Promise} a promise that resolves to the content.
         */
        sync: function(callback) {
            var client = contentful.createClient(options.apiconfig);

            var db = {
                contentTypes: [],
                entries: {},
                space: {}
            };

            var promise = q.all([
                client.contentTypes(),
                client.space()
            ]).then( function(response) {
                var contentTypes = response[0];
                var space = response[1];

                db.space = space;
                db.contentTypes = contentTypes;

                // Loop over all locales and fetch them one at a time
                // FIXME: Option for exactly which I like to fetch.
                var queries = [];
                space.locales.forEach(function(locale) {
                    queries.push(client.entries({ locale: locale.code, limit: 1000}));
                });

                return q.all(queries).then( function(response) {
                    space.locales.forEach(function(locale, index) {
                        var entries = response[index];
                        db.entries[locale.code] = entries;
                    });

                    return db;
                });
            });

            if (callback) {
                promise.then(function(db) {
                    process.nextTick(function() {
                        callback(undefined, db);
                    });
                }, callback);
            }
            return promise;
        },

        /**
         * Sync assets from contentful. You can choose to supply a callback or just use the promise
         * that is returned.
         *
         * @param {Function} callback (optional) a callback function(err, content)
         * @return {Promise} a promise that resolves to the content.
         */
        syncAssets: function(callback) {
            var client = contentful.createClient(options.apiconfig);

            var db = {
                contentTypes: [],
                entries: {},
                space: {}
            };

            var promise = q.all([
                client.assets({ limit : 1000})
            ]).then( function(response) {
                return response[0];
            });

            if (callback) {
                promise.then(function(db) {
                    process.nextTick(function() {
                        callback(db);
                    });
                }, callback);
            }
            return promise;
        },

        /**
         * Saves content to contentful management API.
         *
         * @param {Object} content The content.
         * @param {Function} callback (optional) a callback function(err, html)
         * @return {Promise} that resolves to an object with id of entry as key and HTML as value.
         */
        updateEntry: function(content, callback) {
            var deferred = q.defer();
            var client = contentfulMan.createClient(options.apiconfigManagement);

            var spaceId = client.options.space;
            var entryId = content.sys.id;
            var locale = content.sys.locale;

            client.getSpace(spaceId).then(function(response){
                var space = response;

                space.getEntry(entryId)
                    .then(function(entry) {
                        entry.fields.title = { 'en-US' : content.fields.title};
                        entry.fields.thumbUrl = { 'en-US' : content.fields.thumbUrl};

                        space.updateEntry(entry).then(function(){
                            deferred.resolve();
                        }).catch(function(err) {
                          deferred.reject(err);
                        });
                    })
                    .catch(function(err) {
                        deferred.reject(err);
                    });

            });
            return deferred.promise;
        },

        /**
         * Renders HTML snippets for all entries.
         *
         * @param {Object} content The content.
         * @param {Function} callback (optional) a callback function(err, html)
         * @return {Promise} that resolves to an object with id of entry as key and HTML as value.
         */
        render: function(content, before, callback) {
            // manually setup nunjucks to not cache templates since consolidate doesn't support this option
            consolidate.requires.nunjucks = nunjucks.configure();

            consolidate.requires.nunjucks.addGlobal('getFilename', function(media) {
                if (media !== undefined && media.fields.file !== undefined ) {
                    return media.sys.id + '_' + media.sys.revision + '_' + media.fields.file.fileName;
                }
                return null;
            });

            nunjucksDate.install(consolidate.requires.nunjucks); // install date
            markdown.register(consolidate.requires.nunjucks, marked);

            // expose consolidate to allow for a custom setup
            if(before != undefined) before(consolidate);


            // Massage the data for some easy lookup
            var contentTypes = {};
            content.contentTypes.reduce(function(types, ct) {
              types[ct.sys.id] = ct;
              return types;
            }, contentTypes);


            // FIXME: Only specified locale.
            var renderPromise = q.all(content.space.locales.map(function(locale) {

                // Massage the data for some easy lookup
                var entries = {};
                content.entries[locale.code].reduce(function(entries, entry) {
                  entries[entry.sys.id] = entry;
                  return entries;
                }, entries);

                // Find out order to render in.
                var recurse = function(obj, list, contentTypes) {
                  // Render children first
                  if (Array.isArray(obj)) {
                    obj.forEach(function(item) {
                      recurse(item, list, contentTypes);
                    });
                  } else if (obj && typeof obj === 'object') {
                    Object.keys(obj).forEach(function(k) {
                      if (k !== '_sys' && k !== 'sys') {
                        recurse(obj[k], list, contentTypes);
                      }
                    });
                  }

                  // Then render current entry, if its an entry
                  // It's an entry to us if it has a sys.contentType
                  if (obj && obj.sys && obj.sys.contentType) {
                    list.push({
                      id: obj.sys.id,
                      filename: obj.fields && obj.fields.id || obj.sys.id,
                      name: obj.fields && obj.fields.name || obj.sys.id,
                      contentType: contentTypes[obj.sys.contentType.sys.id].name,
                      entry: obj
                    });
                  }
                };
                var toRender = [];
                recurse(entries, toRender, contentTypes);
                var debugTemplate = function(e) {
                    return '<h4>No template found</h4><pre>' + JSON.stringify(e, undefined, 2) + '</pre>';
                };


                // Awesome! Let's render them, one at a time and include the rendered html in the context
                // of each so that they can in turn include it themselves.
                var render = function(entryObj, includes) {
                  var deferred = q.defer();
                    // Try figuring out which template to use
                    var exists = function(pth) {
                        try {
                            // fs.accessSync(pth);
                            fs.existsSync(pth);
                            return true;
                        } catch (e) {
                            return false;
                        }
                    };

                    // Try a nested path
                    var tmp = entryObj.contentType.split('-');
                    tmp[tmp.length - 1] = tmp[tmp.length - 1] + '.html';
                    var tmpl = path.join.apply(path, tmp);
                    if (!exists(path.join(options.templates,tmpl))) {
                        tmpl = entryObj.contentType + '.html';
                    }


                    // Ok let's check again (TODO: DRY)
                     if (!exists(path.join(options.templates, tmpl))) {
                        console.log('Could not find template ', path.join(options.templates,tmpl));
                        deferred.resolve('<span>(Missing template)</span>');
                    } else if (entryObj.contentType.indexOf('nyt-') > -1 || entryObj.contentType.indexOf('external-article') > -1 ) {
                        deferred.reject();
                    } else {

                        consolidate[options.engine](
                          path.join(options.templates, tmpl),
                          {
                            entry: entryObj.entry,
                            content: content,
                            entries: entries,
                            includes: includes,
                            contentTypes: contentTypes,
                            debug: function(obj) { return JSON.stringify(obj, undefined, 2); },
                                  include: function(obj) {
                                      if (Array.isArray(obj)) {
                                          return obj.map(function(e) {
                                              if (e && e.sys) {
                                                  return includes[e.sys.id]  || debugTemplate(e);
                                              }
                                              return debugTemplate(e);
                                          }).join('\n');
                                      } else if (obj.sys) {
                                          return includes[obj.sys.id]  || debugTemplate(obj);
                                      }
                                  }
                          },
                          function(err, html) {
                            if (err) {
                                console.log('consolidate error', err);
                                deferred.reject(err);
                            } else {

                              deferred.resolve(html);
                            }
                          }
                        );
                    }
                  return deferred.promise;
                };

                var includes = {};
                var promise = toRender.reduce(function(soFar, e) {
                  return soFar.then(function(includes) {
                    return render(e, includes).then(function(html) {
                      if (e.contentType === 'index') {
                        includes['index'] = html;
                      } else if (e.contentType === 'showcasedetail' || e.contentType === 'product' ) {
                        includes[ e.contentType + '-' + e.entry.fields.ref.fields.slug] = html;
                      } else if (e.entry.fields.slug !== undefined ) {
                        includes[e.entry.fields.slug] = html;
                      } else {
                        includes[e.id] = html;
                      }

                      return includes;
                    })
                    .catch(function (err) {
                        // console.log('catch', err);
                        return includes;
                    });
                  });
                }, q(includes));

                return promise;
            })).then(function(results) {
                // Re-map the data to each locale
                var byLocale = {};
                content.space.locales.forEach(function(l, index) {
                    byLocale[l.code] = results[index];
                });
                return byLocale;
            });

            if (callback) {
                renderPromise.then(function(includes) {
                    process.nextTick(function() {
                        callback(undefined, includes);
                    });
                }, callback);
            }
            return renderPromise;
        }
    };

    return contentfulStatic;

})();
